# go-httpd

Simple develompent http server designed to serve local files over http://localhost.

## Usage

```bash
$ ./httpd --help
  -port int
        port to run the server on (default 3000)
  -root string
        specify path to serve over http (default ".")
```

## Build

- Git clone
- go mod tidy
- make build

## Release builds

```bash
$ make release
go build -ldflags="-s -w" -o httpd main.go
```

## Cross-compiling

```bash
$ make build-win 
GOARCH=amd64 GOOS=windows go build -o httpd-windows main.go

$ make build-linux
GOARCH=amd64 GOOS=linux go build -o httpd-linux main.go

$ make build-mac
GOARCH=amd64 GOOS=darwin go build -o httpd-darwin main.go
```

See `Makefile` for more targets.
