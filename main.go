package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
)

func main() {
	rootpath := flag.String("root", ".", "specify path to serve over http")
	httpdport := flag.Int("port", 3000, "port to run the server on")
	flag.Parse()

	fs := http.FileServer(http.Dir(*rootpath))
	http.Handle("/", fs)

	log.Printf("Listening on http://localhost:%d...", *httpdport)
	err := http.ListenAndServe(fmt.Sprintf(":%d", *httpdport), nil)
	if err != nil {
		log.Fatal(err)
	}
}
