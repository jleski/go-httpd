BINARY_NAME=httpd
RELEASE_LDFLAGS=-s -w
.PHONY: default
default: build ;

build-mac:
	GOARCH=amd64 GOOS=darwin go build -o ${BINARY_NAME}-darwin main.go
build-linux:
	GOARCH=amd64 GOOS=linux go build -o ${BINARY_NAME}-linux main.go
build-win:
	GOARCH=amd64 GOOS=windows go build -o ${BINARY_NAME}-windows main.go
build:
	go build -o ${BINARY_NAME} main.go

run:
	go run main.go

release-mac:
	GOARCH=amd64 GOOS=darwin go build -ldflags=${RELEASE_LDFLAGS} -o ${BINARY_NAME}-darwin main.go
release-linux:
	GOARCH=amd64 GOOS=linux go build -ldflags=${RELEASE_LDFLAGS} -o ${BINARY_NAME}-linux main.go
release-win:
	GOARCH=amd64 GOOS=windows go build -ldflags=${RELEASE_LDFLAGS} -o ${BINARY_NAME}-windows main.go
release:
	go build -ldflags="${RELEASE_LDFLAGS}" -o ${BINARY_NAME} main.go

clean:
	go clean
	rm ${BINARY_NAME}
	rm ${BINARY_NAME}-darwin
	rm ${BINARY_NAME}-linux
	rm ${BINARY_NAME}-windows